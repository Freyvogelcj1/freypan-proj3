// This is the first Indexedpages class using the ??? Algorithms
class IndexedPages(val pageList: scala.collection.mutable.ListBuffer[Page] = scala.collection.mutable.ListBuffer.empty[Page]) extends Seq[Page] with Weighted[Page]{
  def iterator() = { pageList.iterator } // For Seq
  def getItems() = { pageList } // For Weighted
  def apply(idx: Int): Page = { pageList(idx) } // For Seq
  def length(): Int = { pageList.length } // For Seq
  def getWeights(): Seq[Double] = { pageList.map(x => 1.0) } // For Weighted
  def add(page: Page): scala.collection.mutable.Seq[Page] = {
    if (!pageList.contains(page)){
      pageList += page
    }
    pageList
  }
  def getURLs(): Seq[String] = {
    pageList.map(x => x.url)
  }
  def search(query: Query): SearchResults = {
    val k = new SearchResults(query,this.length(),this.getURLs,this.getWeights)
    k
  }
}

// Subclass 1 : Colin : Weights URLs based on the number of /.../ directories it goes through
class IndexedPagesURLLength(override val pageList: scala.collection.mutable.ListBuffer[Page] = scala.collection.mutable.ListBuffer.empty[Page]) extends IndexedPages(pageList) {
  override def getWeights(): Seq[Double] = {
    val regexNoURL = raw"""(?:http[s]{0,1}://){0,1}([\w\s\-/\.]+)""".r
    def weightingFn(page: Page): Double  = {
      regexNoURL.findFirstMatchIn(page.url).getOrElse(None) match {
        case None => -1.0
        case x: scala.util.matching.Regex.Match => 1.0 / x.group(1).split('/').length
      }
    }
    getItems.map(weightingFn(_))
  }
  override def search(query: Query): SearchResults = {
    def logOfInverse(s: String): Double = { scala.math.log(length.toDouble / getItems.count((x: Page) => x.tokens.contains(s) )) }
    def getTermFreqScore(sd: (String,Double), pd: (Page,Double)): Double = { if (pd._1.tokens.contains(sd._1)) pd._1.tokens(sd._1).toDouble * sd._2 else 0.0 }
    val zipWeights = getItems.zip(getWeights)
    val inverseDocumentFrequency: Seq[(String,Double)] = query.zip(query.map((s: String) => logOfInverse(s) ) )
    val termFrequency = zipWeights.map((x:(Page,Double))=> x._2 * inverseDocumentFrequency.foldLeft(0.0)((d: Double, s: (String,Double)) => d + getTermFreqScore(s,x) ) )
    val k = new SearchResults(query,this.length(),this.getURLs,termFrequency)
    k
  }
}

// Subclass 2 : Tim : Weights pages based on number of times the page is linked elsewhere
class IndexedPagesNumPages(override val pageList: scala.collection.mutable.ListBuffer[Page] =  scala.collection.mutable.ListBuffer.empty[Page]) extends IndexedPages(pageList) {
	override def getWeights(): Seq[Double] = {
		def weightingFn(page: Page): Double = {
			(for(x <- getItems) yield numLinks(page, x.links)/100).toList.sum
		}
		
		def numLinks(p: Page, l: Set[String]): Double = {
			var temp: Double = 0.0
			for(x <- l){
				if((((p.url.split("/"))(2)).split("\\."))(0) == (((x.split("/"))(2)).split("\\."))(1)){
					temp += 1.0
				}
			}
			temp
		}
		
		getItems.map(weightingFn(_))
	}	
	
	override def search(query: Query): SearchResults = {
		def logOfInverse(s: String): Double = { scala.math.log(length.toDouble / getItems.count((x: Page) => x.tokens.contains(s) )) }
		def getTermFreqScore(sd: (String,Double), pd: (Page,Double)): Double = { if (pd._1.tokens.contains(sd._1)) pd._1.tokens(sd._1).toDouble * sd._2 else 0.0 }
		val zipWeights = getItems.zip(getWeights)
		val inverseDocumentFrequency: Seq[(String,Double)] = query.zip(query.map((s: String) => logOfInverse(s) ) )
		val termFrequency = zipWeights.map((x:(Page,Double))=> x._2 * inverseDocumentFrequency.foldLeft(0.0)((d: Double, s: (String,Double)) => d + getTermFreqScore(s,x) ) )
		val k = new SearchResults(query,this.length(),this.getURLs,termFrequency)
		k
  }
}
