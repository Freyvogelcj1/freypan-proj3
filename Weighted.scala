trait Weighted[A] {

  def getItems: Seq[A]
  def getWeights: Seq[Double]

  // Implemented by Colin
  def sumIf(f: A => Boolean): Double = {
   getItems.zip(getWeights).filter((x: (A,Double)) => f(x._1)).foldLeft(0.0)(_+_._2) // This is really odd functionality that has somethign to do with the interaction between zip and multiple actions
  }
}
