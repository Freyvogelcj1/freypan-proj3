class Query(val query: Seq[String]) extends Seq[String] with Weighted[String] {
  def getItems(): Seq[String] = {query}
  def getWeights(): Seq[Double] = {query.map(x => 1.0)}
  def iterator() = {query.iterator}
  def apply(idx: Int): String = {query(idx)}
  def length(): Int = {query.length}
}

// TODO: Add a subclass of query that implements some others method of 

//class 
class QuerySub(override val query: Seq[String]) extends Query(query) {
  val stopWords = new StopWords()
  override def getWeights(): Seq[Double] = { query.map(x => if (stopWords(x)) 0.0 else weightingFn(x)) }
  def weightingFn(s: String): Double = {
    1.0
  }
}
